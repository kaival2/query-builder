@extends('layouts/main')


@section('container')
    <section class="content">
        <form action="/store" method="post" role="form">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Name : </label>
                    <input type="txt" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="age">Age : </label>
                    <input type="number" class="form-control" id="age" name="age">
                </div>
                <div class="form-group">
                    <label for="validationTextarea">Bio : </label>
                    <textarea class="form-control is-invalid" name="bio" id="validationTextarea"
                        placeholder="Required example textarea" required></textarea>
                    <div class="invalid-feedback">
                        Please enter a bio in the textarea.
                    </div>
                </div>

            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </form>
    </section>

@endsection
