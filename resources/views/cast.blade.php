@extends('layouts/main')

@section('container')

    <section class="content">
        <a href="cast/create"><button type="button" class="btn btn-primary mt-3">Create +</button></a>


        <section class="content">
            <form action="/cast" method="get" role="form">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Search by id number : </label>
                        <input type="number" class="form-control" id="name" name="id">
                    </div>
                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                </div>
                <!-- /.card-body -->
            </form>
        </section>


        @php
            if (isset($_GET['submit'])) {
                $id = $_GET['id'];
            }
        @endphp


        <?php foreach ($cast as $c) : ?>
        <div class="card mb-3 mt-3" style="max-width: 540px;">
            <div class="row no-gutters">
                <div class="col-md-8">

                    <div class="card-body">
                        <h1><?= $c->nama ?></h1>
                        <p class="card-text"><?= $c->bio ?></p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </section>

@endsection()
