<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function index()
    {

        $cast = DB::table('cast')->get();

        return view('cast', ['cast' => $cast, 'title' => 'Cast']);
    }

    public function create()
    {
        return view('create', [
            'title' => 'Create', 'title' => 'Create'
        ]);
    }

    public function store(Request $request)
    {

        // dd($request->all());
        $query = DB::table('cast')->insert([
            "nama" => $request['name'],
            "umur" => $request['age'],
            "bio" => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('cast')
            ->where('id', '=', $id)
            ->get();

        return view('castbyid', ['cast' => $cast, 'title' => "Cast"]);
    }
}
